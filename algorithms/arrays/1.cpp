/**
*
* How do you find the missing number in a given integer array of 1 to 100?
*
* --- QUESTIONS ---
* Is there only one missing number?
* Is the data sorted?
* 
*
*/

#include <iostream>
#include <numeric>
#include <vector>

// Technique #1
constexpr int missingNumber(const std::vector<int> &data) noexcept
{
    const int MAX_SUM = 5050; // n(n + 1)/2 where n = 100.
    return MAX_SUM - std::accumulate(data.begin(), data.end(), 0);
}

int main(int argc, char *argv[])
{
    int vector<int> data {100, 60};
    
    missingNumber(data);

    return 0;
}